package br.com.project.controlinvestment;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.SimpleFormatter;

import br.com.project.controlinvestment.dao.CategoriaRendaDAO;
import br.com.project.controlinvestment.dao.DespesaDAO;
import br.com.project.controlinvestment.dao.GenericDAO;
import br.com.project.controlinvestment.dao.RendaDAO;
import br.com.project.controlinvestment.dao.RendaTipoDAO;
import br.com.project.controlinvestment.dao.UserDAO;
import br.com.project.controlinvestment.entity.*;

/**
 * Hello world!
 *
 */
public class AppRevJPAProject 
{
    public static void main( String[] args )
    {
    	//inserUserDespesa();
    	//removeDespesa();
        //insertUser();
    	//updateUser();
    	updateRendaByUser();
    	//insertRendaByUser();
    	//removeRenda();
    	//insertUserByRenda();
    	//insertUserWithRenda();
    	//creatUserByRenda();
    	//insertDespesa();
    	//updateUserByDespesa();
    	//createTypeRenda();
    	//createCategoriaRenda();
    	
    }

	private static void updateRendaByUser() {
		User user = new UserDAO().findById(2L);
		RendaTipo tipo = new RendaTipoDAO().findById(1L);
		Set<Renda> rendas = user.getRendas();
		for (Renda renda : rendas) {
			renda.setTipo(tipo);
		}
		new UserDAO().update(user);
		
	}

	private static void removeRenda() {
		User user  = new UserDAO().findById(2L);
		Renda renda = new RendaDAO().findById(2L);
		Set<Renda> rendas = user.getRendas();
		rendas.remove(renda);
		
		new UserDAO().update(user);
		
		
	}

	private static void createCategoriaRenda() {
		CategoriaRenda cat2 = new CategoriaRenda("Salário", "Recebimento de salário");
		
		CategoriaRendaDAO dao = new CategoriaRendaDAO();
		dao.save(cat2);
	}

	private static void createTypeRenda() {
		
		RendaTipo tipo = new RendaTipo("Salário", "Recebimento mensal");
		RendaTipo tipo1 = new RendaTipo("Tesouro Nacional", "Investimento em Titulos Públicos");
		RendaTipo tipo2 = new RendaTipo("Bolsa de Valores", "Investimento em Ações");
		
		RendaTipoDAO dao = new RendaTipoDAO();
		dao.save(tipo);
		dao.save(tipo1);
		dao.save(tipo2);
	}

	private static void creatUserByRenda() throws ParseException {
		
		User user = new User("Zécarlos", "Moreira", "zecarlos.moreira@hotmail.com", null);
		Calendar dataNascimento = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		dataNascimento.setTime(format.parse("12/03/1984"));
		user.setDataNascimento(dataNascimento);
		
		Renda renda = new Renda(null, new BigDecimal("234.65"));
		
		user.addRenda(renda);
		
		new RendaDAO().save(renda);
		
	}

	private static void insertUserWithRenda() throws ParseException {
		
		User user = new User("Matias", "Figueredo", "matias.figuereido@gmail.com", null);
		
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		calendar.setTime(format.parse("11/12/1954"));
		
		user.setDataNascimento(calendar);
		
		Renda renda = new Renda(null, new BigDecimal("105.5"));
		
		user.addRenda(renda);
		
		new UserDAO().save(user);
		
		
	}

	private static void insertUserByRenda() {

		Renda renda = new Renda(null, new BigDecimal("560.35"));
		renda.setUser(new UserDAO().findById(1L));
		
		new RendaDAO().save(renda);
		
	}

	private static void insertRendaByUser() {
		
		User user = new UserDAO().findById(2L);
		
		Renda renda = new Renda();
		renda.setValor(new BigDecimal("5000"));
		user.addRenda(renda);
		
		new UserDAO().update(user);
		
	}

	private static void updateUser() {

		User user = new UserDAO().findById(4L);
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy", new Locale("pt", "BR"));
		try {
			calendar.setTime(format.parse("19/16/1990"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		user.setDataNascimento(calendar);
		
		new UserDAO().update(user);
		
	}

	private static void updateUserByDespesa() {
		Despesa despesa = new DespesaDAO().findById(3L);
		
		User user = despesa.getUser();
		
		
		
		new DespesaDAO().delete(3L);
		
	}

	private static void insertDespesa() {
		User user = new UserDAO().findById(1L);
		
		Despesa despesa2 = new Despesa();
		Calendar data1 = Calendar.getInstance();
		despesa2.setData(data1);
		despesa2.setDescricao("Descrição da Despesa 3");
		despesa2.setValorTotal(new BigDecimal("4500"));
		
		for (Despesa despesa : user.getDespesas()) {
			System.out.println("1 - " + despesa.toString());
		}
		
		user.addDespesa(despesa2);
		
		new UserDAO().update(user);
		
		System.out.println(user.toString());
	}

	private static void removeDespesa() {
		User user = new UserDAO().findById(1L);
		
		for (Despesa despesa : user.getDespesas()) {
			if (despesa.getId() == 3) {
				despesa.setDescricao("Descrição da Despesa");
			}
		}
		
		new UserDAO().update(user);
	}

	private static void inserUserDespesa() {
		
		User usr1 = new User();
		usr1.setNome("Maria");
		usr1.setSobrenome("Socorro");
		usr1.setEmail("maria.socorro@hotmail.com");
		
		String dataString = "25/11/1962";
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy", new Locale("pt", "BR"));		
		Calendar data = Calendar.getInstance();
		try {
			data.setTime(format.parse(dataString));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		usr1.setDataNascimento(data);
		
		Despesa despesa = new Despesa();
		Calendar data1 = Calendar.getInstance();
		despesa.setData(data1);
		despesa.setDescricao("Descrição da Despesa numero treis");
		despesa.setValorTotal(new BigDecimal("150"));
		
		Despesa despesa2 = new Despesa();
		despesa2.setData(data1);
		despesa2.setDescricao("Descrição da Despesa numero treis");
		despesa2.setValorTotal(new BigDecimal("200"));
		
		usr1.addDespesa(despesa);
		usr1.addDespesa(despesa2);
		
		new UserDAO().save(usr1);
		
		System.out.println(usr1.toString());
		
	}

	private static void insertUser() {
		
		User user1 = new User();
		user1.setNome("Matilde");
		user1.setSobrenome("Oliveira");
		user1.setEmail("matilde@hotmail.com");
		Calendar data = Calendar.getInstance();
		data.set(1990, Calendar.MAY, 19);
		user1.setDataNascimento(data);
		
		
		new UserDAO().save(user1);
		
	}
}
