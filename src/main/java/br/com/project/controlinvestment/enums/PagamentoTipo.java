package br.com.project.controlinvestment.enums;

public enum PagamentoTipo {

	DINHEIRO, CARTAODEBITO, CARTAOCREDITO, CHEQUE
	
}
