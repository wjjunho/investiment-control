package br.com.project.controlinvestment.dao;

import br.com.project.controlinvestment.entity.TipoDespesa;

public class TipoDespesaDAO extends GenericDAO<TipoDespesa> {

	protected TipoDespesaDAO() {
		super(TipoDespesa.class);
	}

}
