package br.com.project.controlinvestment.dao;

import br.com.project.controlinvestment.entity.Despesa;

public class DespesaDAO extends GenericDAO<Despesa> {

	public DespesaDAO() {
		super(Despesa.class);
	}

}
