package br.com.project.controlinvestment.dao;

import br.com.project.controlinvestment.entity.Renda;

public class RendaDAO extends GenericDAO<Renda> {

	public RendaDAO() {
		super(Renda.class);
	}
}
