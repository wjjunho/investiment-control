package br.com.project.controlinvestment.dao;

import br.com.project.controlinvestment.entity.User;

public class UserDAO extends GenericDAO<User> {

	public UserDAO() {
		super(User.class);
	}
	
}
