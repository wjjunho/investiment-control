package br.com.project.controlinvestment.dao;

import br.com.project.controlinvestment.entity.RendaTipo;

public class RendaTipoDAO extends GenericDAO<RendaTipo> {

	public RendaTipoDAO() {
		super(RendaTipo.class);
	}
}
