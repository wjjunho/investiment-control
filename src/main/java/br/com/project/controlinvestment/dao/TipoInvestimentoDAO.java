package br.com.project.controlinvestment.dao;

import br.com.project.controlinvestment.entity.TipoInvestimento;

public class TipoInvestimentoDAO extends GenericDAO<TipoInvestimento> {

	protected TipoInvestimentoDAO() {
		super(TipoInvestimento.class);
	}

}
