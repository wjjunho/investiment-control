package br.com.project.controlinvestment.dao;

import br.com.project.controlinvestment.entity.FormaPagamento;

public class FormaPagamentoDAO extends GenericDAO<FormaPagamento> {

	protected FormaPagamentoDAO() {
		super(FormaPagamento.class);
	}

}
