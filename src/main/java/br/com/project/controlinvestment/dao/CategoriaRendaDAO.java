package br.com.project.controlinvestment.dao;

import br.com.project.controlinvestment.entity.CategoriaRenda;

public class CategoriaRendaDAO extends GenericDAO<CategoriaRenda> {

	public CategoriaRendaDAO() {
		super(CategoriaRenda.class);
	}

}
