package br.com.project.controlinvestment.dao;

import br.com.project.controlinvestment.entity.Investimento;

public class InvestimentoDAO extends GenericDAO<Investimento> {

	protected InvestimentoDAO() {
		super(Investimento.class);
	}

}
