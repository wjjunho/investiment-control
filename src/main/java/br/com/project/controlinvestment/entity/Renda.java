package br.com.project.controlinvestment.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="TB_RENDA", schema = "controlinvestment")
@Data
public class Renda implements Serializable{

	@Id
	@Column(name = "ID_RENDA")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "ID_RENDATIPO",
			foreignKey = @ForeignKey(name = "FK_RENDATIPO_ID")
	)
	private RendaTipo tipo;
	
	@Column(name="VALOR", nullable=false)
	private BigDecimal valor;
	
	@ManyToOne(cascade = {CascadeType.MERGE})
	@JoinColumn(name = "ID_USUARIO",
			foreignKey = @ForeignKey(name = "FK_USUARIO_ID")
	)
	private User user;

	public Renda() {}

	public Renda(RendaTipo tipo, BigDecimal valor) {
		this.tipo = tipo;
		this.valor = valor;
	}

}
