package br.com.project.controlinvestment.entity;

import br.com.project.controlinvestment.enums.ETipoPagamento;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.PositiveOrZero;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;


@Entity
@Table(name="TB_DESPESA", schema = "controlinvestment")
@Data
public class Despesa implements Serializable{

	@Id
	@Column(name = "ID_DESPESA")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Column(name="ID_TIPO_DESPESA")
	@ManyToOne
	private TipoDespesa tiposDespesa;

	@Column(name="VALOR_TOTAL")
	private BigDecimal valorTotal;
	
	@Column(name="DATA", nullable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar data;
	
	@Column(name="DESCRICAO", length=256)
	private String descricao;

	@Enumerated
	@Column(name = "TIPO_PAGAMENTO")
	private ETipoPagamento eTipoPagamento;

	@Column(name = "PARCELA")
	@PositiveOrZero(message = "Informe uma parcela valida.")
	private Integer parcela;

	@Column(name = "VALOR_PARCELA")
	private BigDecimal valorParcela;
	
	@ManyToOne(cascade = {CascadeType.MERGE})
	@JoinColumn(name = "ID_USUARIO")
	private User user;

	public Despesa(BigDecimal valorTotal, Calendar data, String descricao) {
		this.valorTotal = valorTotal;
		this.data = data;
		this.descricao = descricao;
	}

	public Despesa() {}
}
