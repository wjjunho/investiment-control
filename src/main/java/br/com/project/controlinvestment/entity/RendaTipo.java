package br.com.project.controlinvestment.entity;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Table(name="TB_RENDA_TIPO", schema = "controlinvestment")
@Entity
@Data
public class RendaTipo implements Serializable{

	@Id
	@Column(name = "ID_RENDA_TIPO", unique = true)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="NOME")
	@Length(max=100, min=2, message="Informe um nome de tamanho valido!")
	private String nome;
	
	@Column(name="DESCRICAO", length=256)
	@Length(max=256, message="Informe um tamanho de no maximo {max} e de no minimo {min}.")
	private String descricao;
	
	@ManyToOne(cascade = {CascadeType.MERGE})
	@JoinColumn(name = "ID_CATEGORIA_TIPO")
	private CategoriaRenda categoriaRenda;
	
	@OneToMany(cascade = CascadeType.MERGE)
	private Set<Renda> rendas;
	
	public RendaTipo(String nome, String descricao) {
		this.nome = nome;
		this.descricao = descricao;
	}
}
