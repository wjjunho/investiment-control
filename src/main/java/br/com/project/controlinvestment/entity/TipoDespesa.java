package br.com.project.controlinvestment.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "TB_TIPODESPESA", schema = "controlinvestment")
@Data
public class TipoDespesa implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID_TIPO_DESPESA")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "NOME", nullable = false)
	@Length(max = 40, message = "Insira um valor entre {min} e {max}.")
	private String nome;

	@Column(name = "PERCENTUAL_RECEITA")
	private Double percentualReceita;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<Investimento> investimento;

	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name = "ID_DESPESA")
	private Despesa despesa;

	public TipoDespesa() {}

	public TipoDespesa(String nome, Double percentualReceita) {
		this.nome = nome;
		this.percentualReceita = percentualReceita;
	}
}
