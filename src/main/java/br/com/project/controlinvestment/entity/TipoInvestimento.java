package br.com.project.controlinvestment.entity;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name="TB_TIPOINVESTIMENTO")
@Data
public class TipoInvestimento implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_TIPO_INVESTIMENTO")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="NOME", nullable=false)
	@Length(max=40, min=3, message="Insira no minimo {min} e no maximo {max}")
	private String nome;
	
	@OneToMany(mappedBy = "tipoInvestimento", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
	private List<Investimento> investimentos;
	
	public TipoInvestimento() {
		super();
	}

	public TipoInvestimento(String nome) {
		super();
		this.nome = nome;
	}
}
