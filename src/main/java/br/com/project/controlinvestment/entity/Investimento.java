package br.com.project.controlinvestment.entity;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

@Entity
@Table(name="TB_INVESTIMENTO", schema = "controlinvestment")
@Data
public class Investimento implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="ID_INVESTIMENTO")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
	@JoinColumn(name = "ID_TIPOINVESTIMENTO")
	private TipoInvestimento tipoInvestimento;
	
	@Column(name="NOME")
	@Length(max=40, message="Informe no maximo {max} e no minimo {min}.")
	private String nome;
	
	@Column(name="DATAINICIO")
	private Calendar dataInicio;
	
	@Column(name="DATAFIM")
	private Calendar dataFim;

	public Investimento() {
		super();
	}
	
	public Investimento(String nome, Calendar dataInicio, Calendar dataFim) {
		this.nome = nome;
		this.dataInicio = dataInicio;
		this.dataFim = dataFim;
	}
}
