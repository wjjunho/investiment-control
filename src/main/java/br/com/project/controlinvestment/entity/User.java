package br.com.project.controlinvestment.entity;

import lombok.Data;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Past;
import java.io.Serializable;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="TB_USUARIO", schema = "controlinvestment")
@Data
public class User implements Serializable {
	
	@Id
	@Column(name = "ID_USUARIO", nullable=false, unique=true)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable=false, length=128, name="NOME")
	@NotEmpty(message="Nome obrigatorio!")
	private String nome;
	
	@Column(name = "SOBRENOME", length=128, nullable=false)
	@NotEmpty(message="Sobrenome obrigatório!")
	private String sobrenome;

	@Column(nullable=false, length=128, name="EMAIL", unique=true)
	@Email(message="Insira um email valido!")
	private String email;

	@Past
	@Column(name="DATA_NASCIMENTO")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar dataNascimento;

	@Fetch(FetchMode.SELECT)
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "user",
				fetch = FetchType.EAGER, orphanRemoval = true)
	private Set<Renda> rendas;
	
	@Fetch(FetchMode.SELECT)
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "user", fetch = FetchType.EAGER,
				orphanRemoval = true)
	private Set<Despesa> despesas;

	public User(){}

	public User(String nome, String sobrenome, @Email String email, @Past Calendar dataNascimento){
		this.nome = nome;
		this.sobrenome = sobrenome;
		this.email = email;
		this.dataNascimento = dataNascimento;
	}
	
	public void addRenda(Renda renda) {
		if (rendas == null) {
			rendas = new HashSet<Renda>();
		}
		renda.setUser(this);
		rendas.add(renda);
	}
	
	public void removeRenda(Renda renda) {
		if (rendas != null) {
			rendas.remove(renda);
			renda.setUser(null);
		}
	}

	public void addDespesa(Despesa despesa) {
		if (despesas == null) {
			despesas = new HashSet<Despesa>();
		}
		despesa.setUser(this);
		despesas.add(despesa);
	}
	
	public void removeDespesa(Despesa despesa) {
		if (despesas != null) {
			despesa.setUser(null);
			despesas.remove(despesa);
		}
	}
}
