package br.com.project.controlinvestment.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "TB_CATEGORIA_RENDA", schema = "controlinvestment")
@Data
public class CategoriaRenda implements Serializable{

	@Id
	@Column(name = "ID_CATEGORIA_TIPO")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "NOME")
	private String nome;
	
	@Column(name = "DESCRICAO")
	private String descricao;
	
	public CategoriaRenda(String nome, String descricao) {
		this.nome = nome;
		this.descricao = descricao;
	}
}
	