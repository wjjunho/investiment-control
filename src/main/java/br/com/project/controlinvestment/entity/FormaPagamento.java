package br.com.project.controlinvestment.entity;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name="TB_FORMAPAGAMENTO")
public class FormaPagamento implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID_FORM_PAGAMENTO")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Column(name="NOME", length=45, nullable=false)
	@Length(max=45,message="Insira um valor menor do que {max}!")
	private String nome;
	
	@Column(name="DESCRICAO", length=256)
	@Length(max=256)
	private String descricao;
	
	@Column(name="PARCELA")
	private Integer parcela;
	
	@Column(name = "VALOR")
	private BigDecimal valor;
	
	public FormaPagamento(String nome, String descricao, Integer parcela, BigDecimal valor) {
		this.nome = nome;
		this.descricao = descricao;
		this.parcela = parcela;
		this.valor = valor;
	}
}
